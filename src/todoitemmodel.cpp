/*
 * SPDX-FileCopyrightText: 2021 Sai Moukthik Konduru <saimoukthik777@gmail.com>
 * SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>

 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "todoitemmodel.h"
#include "todoitem.h"

#include <QJsonArray>

ToDoItemModel::ToDoItemModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

ToDoItemModel::ToDoItemModel(const QJsonArray &obj)
{
    for(int i = 0; i<obj.size(); i++){
        m_Items.append(new ToDoItem(obj[i].toObject()));
    }
}

QJsonArray ToDoItemModel::toJson() const
{
    QJsonArray obj;
    int i = 0;
    while(i< m_Items.size()){
        QJsonValue temp;
        temp = m_Items[i]->toJson();
        obj.append(temp);
        i++;
    }
    return obj;
}

QHash<int, QByteArray> ToDoItemModel::roleNames() const
{
    return {{Roles::ToDoItemRole, "todoItem"}};
}

ToDoItem* ToDoItemModel::get(const int &index) const
{
    return m_Items.at(index);
}

void ToDoItemModel::updateItem(int index) {
    Q_EMIT dataChanged(createIndex(index, 0), createIndex(index, 0));
}

void ToDoItemModel::move(int oldIndex, int newIndex)
{
    if (oldIndex < 0 || oldIndex > m_Items.size() || newIndex < 0 || newIndex > m_Items.size())
        return;

    if (oldIndex == newIndex) {
        updateItem(oldIndex);
        return;
    }

    beginMoveRows(QModelIndex(), oldIndex, oldIndex, QModelIndex(), oldIndex < newIndex ? newIndex + 1 : newIndex);

    if (newIndex == m_Items.size()) {
        m_Items.append(m_Items.takeAt(oldIndex));
    } else {
        m_Items.swapItemsAt(oldIndex, newIndex);
    }

    endMoveRows();
}



int ToDoItemModel::completedIndex()
{
    int completeIndex = 0;
    while (completeIndex < m_Items.size() && !m_Items[completeIndex]->done()){
        completeIndex++;
    }
    return completeIndex;
}


void ToDoItemModel::append()
{
    beginInsertRows({}, m_Items.count(), m_Items.count());

    ToDoItem* item = new ToDoItem(this);
    m_Items.append(item);
    emit endInsertRows();
}

void ToDoItemModel::removeCompletedItem()
{
    for (int i = 0; i<m_Items.size();){
        if (m_Items.at(i)->done()){
            beginRemoveRows({}, i, i);
            m_Items.removeAt(i);
            emit endRemoveRows();
        }
        else{
            ++i;
        }
    }
}

int ToDoItemModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_Items.count();
}

QVariant ToDoItemModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row() >= m_Items.count() || index.row() < 0)
        return QVariant();

    auto *item = m_Items.at(index.row());

    if (role == Roles::ToDoItemRole)
        return QVariant::fromValue(item);
    return {};
}


Qt::ItemFlags ToDoItemModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

