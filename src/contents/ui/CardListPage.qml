/*
 * SPDX-FileCopyrightText: 2021 Sai Moukthik Konduru <saimoukthik777@gmail.com>
 * SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>

 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.1
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.3
import ToDo 1.0

Kirigami.ScrollablePage{
    title: "foKus"

    TaskListPage{
        id: taskListPage
        visible: false
    }

    actions{
        main: Kirigami.Action{
            icon.name: "list-add"
            onTriggered: ToDoListModel.appendList();
        }
    }

    property ToDoList currentList
    property int currentListIndex

    ListView{
        id: view
        model: ToDoListModel

        delegate: Kirigami.SwipeListItem{

            onClicked: {
                taskListPage.todoList = model.todoList.list
                applicationWindow().pageStack.push(taskListPage)
            }

            Kirigami.Heading{
                id: listName
                level: 2
                Layout.columnSpan: 3
                text: todoList.name
            }

            actions: [
                Kirigami.Action{
                    text: "EditName"
                    icon.name: "cell_edit"
                    onTriggered: {
                        editDialogName.text = model.todoList.name
                        currentList = model.todoList
                        editNameDialog.open()

                    }
                },

                Kirigami.Action{
                    text: "delete"
                    icon.name: "edit-delete-remove"
                    onTriggered: {
                        currentListIndex = index
                        deleteListDialog.open()
                    }
                }

            ]

        }
    }
    Kirigami.OverlaySheet {
        id: deleteListDialog

        footer: RowLayout {
            Item {
                Layout.fillWidth: true
            }

            Controls.Button {
                flat: false
                text: "No"
                Layout.alignment: Qt.AlignRight
                onClicked: deleteListDialog.close()
            }

            Controls.Button {
                flat: false
                text: "Yes"
                onClicked: {
                    ToDoListModel.removeSelectedList(currentListIndex)
                    deleteListDialog.close()
                }

                Layout.alignment: Qt.AlignRight
            }
        }

        Kirigami.Heading{
            text: "Do you want to delete this list?"
        }
    }
    Kirigami.OverlaySheet {
        id: editNameDialog

        footer: RowLayout {
            Item {
                Layout.fillWidth: true
            }

            Controls.Button {
                flat: false
                text: "Cancel"
                Layout.alignment: Qt.AlignRight
                onClicked: editNameDialog.close()
            }

            Controls.Button {
                flat: false
                text: "Done"
                onClicked: {
                    currentList.name = editDialogName.text
                    editNameDialog.close()
                }

                Layout.alignment: Qt.AlignRight
            }
        }

        GridLayout {
            columns: 2
            rowSpacing: Kirigami.Units.largeSpacing

            Kirigami.Heading {
                text: i18n("Name")
                level: 4
            }
            Controls.TextField {
                id: editDialogName
            }
        }
    }

}
