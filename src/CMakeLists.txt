﻿add_executable(fokus main.cpp todoitem.cpp todoitem.h todoitemmodel.cpp todoitemmodel.h todolistmodel.cpp todolistmodel.h todolist.cpp todolist.h resources.qrc)
target_link_libraries(fokus
    Qt5::Core
    Qt5::Gui
    Qt5::Qml
    Qt5::Quick
    Qt5::QuickControls2
    Qt5::Svg
    KF5::I18n)
install(TARGETS fokus ${KDE_INSTALL_TARGETS_DEFAULT_ARGS})
