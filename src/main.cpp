/*
 * SPDX-FileCopyrightText: 2021 Sai Moukthik Konduru <saimoukthik777@gmail.com>
 * SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>

 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>
#include <KLocalizedContext>
#include <QQmlContext>

#include "todoitemmodel.h"
#include "todolistmodel.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);

    qmlRegisterType<ToDoList>("ToDo", 1, 0, "ToDoList");
    qmlRegisterType<ToDoItem>("ToDo", 1, 0, "ToDoItem");


    QCoreApplication::setOrganizationName("KDE");
    QCoreApplication::setOrganizationDomain("kde.org");
    QCoreApplication::setApplicationName("foKus");

    ToDoList toDoList;

    ToDoListModel toDoListModel;

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextProperty(QStringLiteral("ToDoItemModel"), &toDoList);

    engine.rootContext()->setContextProperty(QStringLiteral("ToDoListModel"), &toDoListModel);


    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
