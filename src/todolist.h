/*
 * SPDX-FileCopyrightText: 2021 Sai Moukthik Konduru <saimoukthik777@gmail.com>
 * SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>

 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef TODOLIST_H
#define TODOLIST_H

#include <QObject>
#include "todoitemmodel.h"

class ToDoList : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY propertyChanged)
    Q_PROPERTY(ToDoItemModel* list READ list WRITE setList NOTIFY propertyChanged)

public:
    explicit ToDoList(QObject *parent = nullptr, QString name = "Tasks", ToDoItemModel* list = nullptr);

    explicit ToDoList(const QJsonObject &obj);

    QJsonObject toJson() const;

    QString name() const
    {
        return m_name;
    }

    ToDoItemModel* list() const
    {
        return m_list;
    }


    void setName(const QString &name);
    void setList(ToDoItemModel* &list);

private:
    QString m_name;
    ToDoItemModel* m_list;
    ToDoItemModel* m_completedList;

signals:
    void propertyChanged();

};

#endif // TODOLIST_H
