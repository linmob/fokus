/*
 * SPDX-FileCopyrightText: 2021 Sai Moukthik Konduru <saimoukthik777@gmail.com>
 * SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>

 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "todoitem.h"

ToDoItem::ToDoItem(QObject *parent, bool done, QString description, bool priority) : QObject(parent)
    ,m_done(done)
    ,m_description(description)
    ,m_priority(priority)
{
    m_description = "Task";
    m_priority = false;
}

ToDoItem::ToDoItem(const QJsonObject &object)
{
    m_done = object["done"].toBool();
    m_description = object["description"].toString();
    m_priority = object["priority"].toBool();
}

QJsonObject ToDoItem::toJson() const
{
    QJsonObject obj;
    obj["done"] = m_done;
    obj["description"] = m_description;
    obj["priority"] = m_priority;
    return obj;
}

void ToDoItem::setDone(const bool &done)
{
    m_done = done;
    emit propertyChanged();
}

void ToDoItem::setDescription(const QString &description)
{
    m_description = description;
    emit propertyChanged();
}

void ToDoItem::setPriority(const bool &priority)
{
    m_priority = priority;
}
;
